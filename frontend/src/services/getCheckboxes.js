import axios from 'axios'

export const getCheckboxes = async () => {
    const response = await axios
    .get('http://127.0.0.1:3000/')
    .catch(error => console.log(error))
    return response.data.settings
}